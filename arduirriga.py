"""
# ArduIrriga - Sistema inteligente de irrigacao
Autores: Alvaro Cafe, Tulio Avelar
-----
Controle da valvula para acionamento automatico. 
Este programa tem como objetivo controlar o acionamento de um valvula hidraulica para o sistema de irrigacao.

"""
import RPi.GPIO as GPIO
import time

"""
beep
"""
def beep(N,pino,TempoAberta,TempoFechada):
    for x in range(0,N):
        GPIO.output(pino,True)
        time.sleep(TempoAberta)
        GPIO.output(pino,False)
        time.sleep(TempoFechada)

pino = 16
TempoAberta = 0.5
TempoFechada = 0.05
GPIO.setmode(GPIO.BOARD)
GPIO.setup(pino,GPIO.OUT)
N=10
beep(N,pino,TempoAberta,TempoFechada)

# for i in range(0,2):
#     for x in range(0,4):
#         GPIO.output(pino,True)
#         time.sleep(TempoAberta)
#         GPIO.output(pino,False)
#         time.sleep(TempoFechada)
#     for x in range(0,4):
#         GPIO.output(pino,True)
#         time.sleep(TempoAberta*0.25)
#         GPIO.output(pino,False)
#         time.sleep(TempoFechada)
#     time.sleep(TempoFechada*4)

GPIO.cleanup()
