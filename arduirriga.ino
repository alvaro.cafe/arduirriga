// ArduIrriga - Sistema inteligente de irrigacao
// Autores: Alvaro Cafe, Tulio Avelar
////////////////////////////////////////////////////////////////////
// Controle da valvula para acionamento automatico. 
// Esse programa foi escrito e testado em um arduino nano.
// Este programa tem como objetivo controlar o acionamento de uma valvula hidraulica para o sistema de irrigacao.

int valvepin = 13;  // Pino cujo sinal controlara a valvula
int tempoaberta=1*1000;  // Tempo em que a valvula estara aberta [ms]
int tempofechada=5*1000;  // Tempo em que a valvula permanecera fechada [ms]
void setup()
{
  pinMode(valvepin, OUTPUT);  // Inicia o pino da valvula como output de sinal
}

void loop()
{ 
  digitalWrite(valvepin, HIGH);  // Abre a valvula
  delay(tempoaberta);  // Espera o tempo em que a valvula permanece aberta
  digitalWrite(valvepin, LOW);  // Fecha a valvula
  delay(tempofechada);  // Espera o tempo em que a valvula permanece fechada
}
