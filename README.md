# arduirriga

Esse é o arduirriga, automação de irrigação inteligente acessível para casas e pequenas comunidades! A partir de 04/08/2018, o arduirriga chegou a sua versão 0.01 e está funcionando para irrigar uma horta com muito amor, duas vezes por dia, faça chuva ou faça sol. Já estamos trabalhando para adicionar a funcionalidade de não irrigar quando fizer chuva.